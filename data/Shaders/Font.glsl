#Vertex
#version 330

layout(location = 0) in float pos;
layout(location = 1) in vec4 color;
layout(location = 2) in int c;

out vec4 vColor;
flat out int vC;

void main()
{
	gl_Position = vec4(pos, 0, 0, 1.0);
	vColor = color;
	vC = c;
}

#Geometry
#version 330

uniform mat4 mvp;

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in vec4 vColor[];
flat in int vC[];

out vec2 texCoord;
out vec4 color;
flat out int c;

void main()
{
	gl_Position = mvp * (gl_in[0].gl_Position);
	color = vColor[0];
	texCoord = vec2(0, 0);
	c = vC[0];
	EmitVertex();

	gl_Position = mvp * (gl_in[0].gl_Position + vec4(0, 1, 0, 0));
	color = vColor[0];
	texCoord = vec2(0, 1);
	c = vC[0];
	EmitVertex();

	gl_Position = mvp * (gl_in[0].gl_Position + vec4(1, 0, 0, 0));
	color = vColor[0];
	texCoord = vec2(1, 0);
	c = vC[0];
	EmitVertex();

	gl_Position = mvp * (gl_in[0].gl_Position + vec4(1, 1, 0, 0));
	color = vColor[0];
	texCoord = vec2(1, 1);
	c = vC[0];
	EmitVertex();

	EndPrimitive();
}

#Fragment
#version 330

uniform sampler2D tex;

in vec4 color;
in vec2 texCoord;
flat in int c;

layout(location = 0) out vec4 outColor;

void main()
{
	int row = c / 16;
	int column = c % 16;

	vec2 minUV = vec2(column / 16.0, row / 16.0);

	vec2 coord = texCoord - floor(texCoord);
	vec4 tmpColor = vec4(1, 1, 1, texture(tex, minUV + coord / 16.0)) * color;

	outColor = tmpColor;
}