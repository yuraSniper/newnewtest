#Vertex
#version 330

uniform mat4 mvp;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in ivec2 texPos;

smooth out vec2 vTexCoord;
smooth out vec4 vColor;
flat out ivec2 vTexPos;

void main()
{
	gl_Position = mvp * vec4(position, 1.0);
	vTexCoord = texCoord;
	vTexPos = texPos;
	vColor = color;
}

#Fragment
#version 330

uniform sampler2D[5] tex;
uniform int[5] texRowCount;

smooth in vec2 vTexCoord;
smooth in vec4 vColor;
flat in ivec2 vTexPos;

layout(location = 0) out vec4 color;

void main()
{
	int row = vTexPos.y / texRowCount[vTexPos.x];
	int column = vTexPos.y % texRowCount[vTexPos.x];

	vec2 minUV = vec2(column / float(texRowCount[vTexPos.x]), row / float(texRowCount[vTexPos.x]));

	vec2 coord = vTexCoord - floor(vTexCoord);
	color = vColor * texture(tex[vTexPos.x], minUV + coord / float(texRowCount[vTexPos.x]));
}