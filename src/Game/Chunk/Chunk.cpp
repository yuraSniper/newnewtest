#include "Chunk.h"

#include <Util\Math\NewNoise\NewNoise.h>

Chunk::Chunk(short posx, short posz) : posX(posx), posZ(posz)
{
	NewNoise noise(4, 1 / 128.);
	for (int x = 0; x < size; x++)
	{
		for (int z = 0; z < size; z++)
		{
			int h = noise.noise(x + posX * size, z + posZ * size) * 16 + 16;
			for (int y = 0; y < h; y++)
				blocks[(((x * size) + z) * height) + y] = 1;
			for (int y = h; y < height; y++)
				blocks[(((x * size) + z) * height) + y] = 0;
		}
	}
}

unsigned int Chunk::getHash(unsigned short x, unsigned short z)
{
	return (x << 16) | z;
}

short Chunk::getBlockId(short x, short y, short z)
{
	/*if (x < 0)
		_asm int 3
	else if (x > 15)
		_asm int 3
	else if (z < 0)
		_asm int 3
	else if (z > 15)
		_asm int 3*/
	return blocks[(((x * size) + z) * height) + y];
}

void Chunk::setBlockId(short id, short x, short y, short z)
{
	/*if (x < 0)
		_asm int 3
	else if (x > 15)
		_asm int 3
	else if (z < 0)
		_asm int 3
	else if (z > 15)
		_asm int 3*/
	blocks[(((x * size) + z) * height) + y] = id;
}