#pragma once

#include <std.h>

class Chunk
{
public:
	static const int size = 16;
	static const int height = 128;
private:
	short blocks[size * size * height];
	short posX, posZ;
public:
	Chunk(short x, short z);
	static unsigned int getHash(unsigned short x, unsigned short z);
	short getBlockId(short x, short y, short z);
	void setBlockId(short id, short x, short y, short z);
};