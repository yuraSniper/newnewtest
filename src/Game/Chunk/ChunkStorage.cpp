#include "ChunkStorage.h"
#include <Game\Chunk\Chunk.h>

shared_ptr<Chunk> ChunkStorage::getChunk(short x, short z)
{
	if (storage.count(Chunk::getHash(x, z)) > 0)
		return storage[Chunk::getHash(x, z)];
	return nullptr;
}

void ChunkStorage::loadChunk(short x, short z)
{
	if (storage.count(Chunk::getHash(x, z)) == 0)
		storage[Chunk::getHash(x, z)] = make_shared<Chunk>(x, z);
}

void ChunkStorage::unloadChunk(short x, short z)
{
	if (storage.count(Chunk::getHash(x, z)) > 0)
		storage.erase(Chunk::getHash(x, z));
}