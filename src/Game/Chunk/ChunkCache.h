#pragma once

#include <std.h>
#include <Game\World\IWorld.h>

class World;

class ChunkCache : public IWorld
{
	shared_ptr<vector<shared_ptr<Chunk>>> chunks;
	bool verifyCoord(short x, int y, short z);
public:
	const short posX, posY, posZ;

	ChunkCache(shared_ptr<World> wld, short x, short y, short z);

	bool isFilled();

	shared_ptr<Chunk> getChunk(short x, short z);
	shared_ptr<Chunk> getChunkFromBlock(int x, int z);
	short getBlockId(int x, int y, int z);
	void setBlockId(short id, int x, int y, int z);
	shared_ptr<vector<shared_ptr<Chunk>>> getNeighborChunks(short x, short z);
};