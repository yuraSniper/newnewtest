#include "ChunkCache.h"
#include <Game\Chunk\Chunk.h>
#include <Game\World\World.h>
#include <Util\Util.h>

bool ChunkCache::verifyCoord(short x, int y, short z)
{
	if (y < 0 || y > Chunk::height || x < 0 || x > 2 || z < 0 || z > 2)
		return false;
	return true;
}

ChunkCache::ChunkCache(shared_ptr<World> wld, short x, short y, short z) : posX(x), posY(y), posZ(z)
{
	chunks = wld->getNeighborChunks(x, z);
}

bool ChunkCache::isFilled()
{
	for (int i = 0; i < 9; i++)
		if ((*chunks)[i] == nullptr)
			return false;
	return true;
}

shared_ptr<Chunk> ChunkCache::getChunk(short x, short z)
{
	x -= posX - 1;
	z -= posZ - 1;
	if (verifyCoord(x, 0, z))
		return (*chunks)[z * 3 + x];
	return nullptr;
}

shared_ptr<Chunk> ChunkCache::getChunkFromBlock(int x, int z)
{
	return getChunk(x / Chunk::size, z / Chunk::size);
}

short ChunkCache::getBlockId(int x, int y, int z)
{
	int xx = x / Chunk::size - posX + 1;
	int zz = z / Chunk::size - posZ + 1;
	if (x < 0)
		xx--;
	if (z < 0)
		zz--;
	if (verifyCoord(xx, y, zz))
		return (*chunks)[zz * 3 + xx]->getBlockId(Util::wrap(x, Chunk::size), y, Util::wrap(z, Chunk::size));
	return -1;
}

void ChunkCache::setBlockId(short id, int x, int y, int z)
{
	int xx = x / Chunk::size - posX + 1;
	int zz = z / Chunk::size - posZ + 1;
	if (x < 0)
		xx--;
	if (z < 0)
		zz--;
	if (verifyCoord(xx, y, zz))
		(*chunks)[zz * 3 + xx]->setBlockId(id, Util::wrap(x, Chunk::size), y, Util::wrap(z, Chunk::size));
}

shared_ptr<vector<shared_ptr<Chunk>>> ChunkCache::getNeighborChunks(short x, short z)
{
	return chunks;
}