#pragma once

#include <std.h>

class Chunk;

class ChunkStorage
{
	unordered_map<unsigned int, shared_ptr<Chunk>> storage;
public:
	shared_ptr<Chunk> getChunk(short x, short z);
	void loadChunk(short x, short z);
	void unloadChunk(short x, short z);
};