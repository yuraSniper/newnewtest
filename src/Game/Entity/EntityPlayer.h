#pragma once

#include <std.h>
#include <Game\Entity\Entity.h>

class EntityPlayer : public Entity
{
public:
	glm::vec3 eyeOffset;

	EntityPlayer();
};