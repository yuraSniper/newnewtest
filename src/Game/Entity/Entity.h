#pragma once

#include <std.h>
#include <glm.h>

class Entity
{
public:
	glm::vec3 relPos;
	glm::ivec3 chunkPos;
	glm::vec3 angles;
};