#pragma once

#include <std.h>
#include "IWorld.h"

class ChunkStorage;

class World : public IWorld
{
	shared_ptr<ChunkStorage> storage;
public:
	World();

	shared_ptr<Chunk> getChunk(short x, short z);
	shared_ptr<Chunk> getChunkFromBlock(int x, int z);
	short getBlockId(int x, int y, int z);
	void setBlockId(short id, int x, int y, int z);
	shared_ptr<vector<shared_ptr<Chunk>>> getNeighborChunks(short x, short z);

	shared_ptr<ChunkStorage> getChunkStorage();
};