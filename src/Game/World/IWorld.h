#pragma once

#include <std.h>

class Chunk;

class IWorld
{
public:
	virtual shared_ptr<Chunk> getChunk(short x, short z) = 0;
	virtual shared_ptr<Chunk> getChunkFromBlock(int x, int z) = 0;
	virtual short getBlockId(int x, int y, int z) = 0;
	virtual void setBlockId(short id, int x, int y, int z) = 0;
	virtual shared_ptr<vector<shared_ptr<Chunk>>> getNeighborChunks(short x, short z) = 0;
};