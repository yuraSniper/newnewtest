#include "World.h"
#include <Game\Chunk\Chunk.h>
#include <Game\Chunk\ChunkStorage.h>

World::World()
{
	storage = make_shared<ChunkStorage>();
}

shared_ptr<Chunk> World::getChunk(short x, short z)
{
	return storage->getChunk(x, z);
}

shared_ptr<Chunk> World::getChunkFromBlock(int x, int z)
{
	return getChunk(x / Chunk::size, z / Chunk::size);
}

short World::getBlockId(int x, int y, int z)
{
	shared_ptr<Chunk> chunk = getChunkFromBlock(x, z);
	if (chunk != nullptr)
		return chunk->getBlockId(x & (Chunk::size - 1), y, z & (Chunk::size - 1));
	return -1;
}

void World::setBlockId(short id, int x, int y, int z)
{
	shared_ptr<Chunk> chunk = getChunkFromBlock(x, z);
	if (chunk != nullptr)
		chunk->setBlockId(id, x & (Chunk::size - 1), y, z & (Chunk::size - 1));
}

shared_ptr<vector<shared_ptr<Chunk>>> World::getNeighborChunks(short x, short z)
{
	shared_ptr<vector<shared_ptr<Chunk>>> neighbors = make_shared<vector<shared_ptr<Chunk>>>(9);
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			(*neighbors)[j * 3 + i] = getChunk(x + i - 1, z + j - 1);
	return neighbors;
}

shared_ptr<ChunkStorage> World::getChunkStorage()
{
	return storage;
}