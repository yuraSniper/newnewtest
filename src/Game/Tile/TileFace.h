#pragma once

enum TileFace
{
	TOP = 0,
	BOTTOM,
	NORTH,
	WEST,
	SOUTH,
	EAST
};