#pragma once

#include "TileFace.h"
#include <std.h>

class IWorld;

class Tile
{
public:
	const short id;

	Tile(string tileId);

	virtual bool isNormal();

	virtual bool shouldRenderFace(IWorld * world, int x, int y, int z, TileFace face);
};