#include "Tile.h"
#include <Game\Registry\TileRegistry.h>
#include <Game\World\IWorld.h>

Tile::Tile(string tileId) : id(TileRegistry::getInstance().registerTile(this, tileId))
{

}

bool Tile::isNormal()
{
	return true;
}

bool Tile::shouldRenderFace(IWorld * world, int x, int y, int z, TileFace face)
{
	//Tile * tile = TileRegistry::getInstance().getTileById(world->getBlockId(x, y, z));
	return world->getBlockId(x, y, z) == 0; //|| (tile != nullptr && !tile->isNormal());
}