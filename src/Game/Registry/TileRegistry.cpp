#include "TileRegistry.h"

TileRegistry & TileRegistry::getInstance()
{
	static TileRegistry reg;
	return reg;
}

void TileRegistry::loadTileList(vector<string> & t)
{
	ids.reserve(t.size());
	copy(t.begin(), t.end(), ids.begin());
}

short TileRegistry::registerTile(Tile * block, string & str)
{
	vector<string>::iterator str1 = find(ids.begin(), ids.end(), str);
	short id = str1 - ids.begin();

	if (id == ids.size())
	{
		ids.push_back(str);
		tiles.push_back(block);
		id++;
	}

	return id;
}

Tile * TileRegistry::getTileById(int id)
{
	if (id == 0 || id == -1)
		return nullptr;
	return tiles[id - 1];
}

Tile * TileRegistry::getTileByString(string & str)
{
	int id = find(ids.begin(), ids.end(), str) - ids.begin();
	return id < ids.size()? tiles[id] : nullptr;
}