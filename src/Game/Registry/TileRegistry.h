#pragma once

#include <std.h>

class Tile;

class TileRegistry
{
	TileRegistry()
	{
	}
	TileRegistry(TileRegistry &);
	void operator=(TileRegistry &);

	vector<Tile *> tiles;
	vector<string> ids;
public:
	static TileRegistry & getInstance();

	void loadTileList(vector<string> & t);
	short registerTile(Tile * tile, string & str);
	Tile * getTileById(int id);
	Tile * getTileByString(string & str);
};