#pragma once

#include <std.h>
#include <Thread\Task\Task.h>
#include <Game\World\World.h>
#include <Game\Chunk\ChunkStorage.h>

class LoadChunkTask : public Task
{
	short x, z;
	weak_ptr<World> wld;
public:
	LoadChunkTask(shared_ptr<World> world, short x, short z)
	{
		this->x = x;
		this->z = z;
		wld = world;
	}

	virtual void operate()
	{
		if (!wld.expired())
			wld.lock()->getChunkStorage()->loadChunk(x, z);
	}

	virtual bool operator<(Task & t)
	{
		return x != reinterpret_cast<LoadChunkTask &>(t).x &&
			z != reinterpret_cast<LoadChunkTask &>(t).z &&
			__super::operator<(t);
	}
};