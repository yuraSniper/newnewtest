#pragma once

#include <Thread\Thread.h>

class GeneralThread : public Thread
{
	bool working = true;

public:
	virtual void run()
	{
		while (working)
		{
			if (crit.tryLock())
			{
				if (!taskQueue.empty())
				{
					(*taskQueue.begin())->operate();
					taskQueue.erase(taskQueue.begin());
				}
				crit.unlock();
			}
		}
	}

	virtual void finalize()
	{
		working = false;
	}
};