#pragma once

#include <std.h>
#include <Util\Delegate.h>

class Window
{
	HWND hWnd;
	HINSTANCE hInst;
	HDC hDC;
	void setupWndClass();
	static LRESULT WndProc(HWND, UINT, WPARAM, LPARAM);
public:
	Delegate onKey, onMouseWheel, onTimer;
	Delegate onCLose;

	Window(int width, int height);
	HWND getHWND();
	HDC getHDC();
	void messageLoop();
	void setVisibility(bool visibility);
	void setCursorVisibility(bool visibility);
	void setTitle(string title);
	bool isVisible();
	bool isCursorVisible();
	bool isActive();
	int getWidth();
	int getHeight();
	void swapBuffers();
};