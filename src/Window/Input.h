#pragma once

#include <std.h>

class Window;

class Input
{
	shared_ptr<Window> wnd;
	bool keysPressed[256];
	unsigned char lastKey;
	int dw;

	static void onKey(void * param, void * key);
	static void onMouseWheel(void * param, void * d);
public:
	Input(shared_ptr<Window> w);

	bool getKey(unsigned char key);
	bool getKeyPressed(unsigned char key);
	void setClientCursorPos(int x, int y);
	void setScreenCursorPos(int x, int y);
	int getX();
	int getY();
	int getMouseWheelDelta();
	bool getMouse(short button);
	bool getMousePressed(short button);
};