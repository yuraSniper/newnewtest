#include "Window.h"

void Window::setupWndClass()
{
	WNDCLASSEX wc = {0};
	wc.cbSize = sizeof(wc);
	wc.hbrBackground = GetStockBrush(WHITE_BRUSH);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hInstance = hInst = GetModuleHandle(nullptr);
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.lpszClassName = "Test Window";

	RegisterClassEx(&wc);
}

LRESULT Window::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int delta;
	Window & This = *(Window *)GetProp(hWnd, "This");
	switch (msg)
	{
		case WM_TIMER:
			This.onTimer(nullptr);
			return 0;

		case WM_KEYDOWN:
		case WM_KEYUP:
			This.onKey(&wParam);
			return 0;

		case WM_MOUSEWHEEL:
			delta = GET_WHEEL_DELTA_WPARAM(wParam);
			This.onMouseWheel(&delta);
			return 0;

		case WM_ERASEBKGND:
			return 0;

		case WM_CLOSE:
		case WM_DESTROY:
			KillTimer(hWnd, 0);
			This.onCLose(nullptr);
			return 0;

		case WM_CREATE:
			SetTimer(hWnd, 0, 0, nullptr);
			return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

Window::Window(int width, int height)
{
	setupWndClass();
	RECT rt, rt1;
	GetWindowRect(GetDesktopWindow(), &rt1);

	rt.left = rt.top = 0;
	rt.right = width;
	rt.bottom = height;
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, 0);
	width = rt.right - rt.left;
	height = rt.bottom - rt.top;

	hWnd = CreateWindow("Test Window", "", WS_OVERLAPPEDWINDOW,
		(rt1.right - width) / 2, (rt1.bottom - height) / 2,
		width, height, nullptr, nullptr, hInst, nullptr);
	SetProp(hWnd, "This", this);

	hDC = GetDC(hWnd);
	PIXELFORMATDESCRIPTOR pfd = {0};
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;

	SetPixelFormat(hDC, ChoosePixelFormat(hDC, &pfd), &pfd);
}

HWND Window::getHWND()
{
	return hWnd;
}

HDC Window::getHDC()
{
	return hDC;
}

void Window::messageLoop()
{
	MSG msg;
	if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void Window::setVisibility(bool visibility)
{
	ShowWindow(hWnd, visibility? SW_SHOW : SW_HIDE);
}

void Window::setCursorVisibility(bool visibility)
{
	ShowCursor(visibility);
}

void Window::setTitle(string title)
{
	SetWindowText(hWnd, title.c_str());
}

bool Window::isVisible()
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(wp);
	GetWindowPlacement(hWnd, &wp);
	return wp.showCmd != SW_HIDE && wp.showCmd != SW_MINIMIZE;
}

bool Window::isCursorVisible()
{
	CURSORINFO ci;
	ci.cbSize = sizeof(ci);
	GetCursorInfo(&ci);
	return ci.flags != 0;
}

bool Window::isActive()
{
	return IsWindowEnabled(hWnd) != 0;
}

int Window::getWidth()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.right - rt.left;
}

int Window::getHeight()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.bottom - rt.top;
}

void Window::swapBuffers()
{
	SwapBuffers(hDC);
}