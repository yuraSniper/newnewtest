#include "Input.h"
#include "Window.h"

void Input::onKey(void * param, void * key)
{
	unsigned char k = *(int *) key;
	switch (k)
	{
		case VK_SHIFT :
			k = GetAsyncKeyState(VK_LSHIFT) ? VK_LSHIFT : VK_RSHIFT;
			break;
		case VK_CONTROL:
			k = GetAsyncKeyState(VK_LCONTROL) ? VK_LCONTROL : VK_RCONTROL;
			break;
		case VK_MENU:
			k = GetAsyncKeyState(VK_LMENU) ? VK_LMENU : VK_RMENU;
			break;
	}
	((Input *) param)->lastKey = k;
}

void Input::onMouseWheel(void * param, void * d)
{
	((Input *)param)->dw += *(int *)d;
}

Input::Input(shared_ptr<Window> w)
{
	wnd = w;
	wnd->onKey.add(onKey, this);
	wnd->onMouseWheel.add(onMouseWheel, this);

	dw = 0;

	for (short i = 0; i < 256; i++)
		keysPressed[i] = false;
}

bool Input::getKeyPressed(unsigned char key)
{
	bool b = GetAsyncKeyState(key) != 0;
	keysPressed[key] |= b;

	if (!b && keysPressed[key])
	{
		keysPressed[key] = false;
		return true;
	}

	return false;
}

bool Input::getKey(unsigned char key)
{
	bool b = GetAsyncKeyState(key) != 0;
	keysPressed[key] |= b;
	return b;
}

int Input::getMouseWheelDelta()
{
	int d = dw;
	dw = 0;
	return d;
}

bool Input::getMouse(short button)
{
	return getKey(VK_LBUTTON + button);
}

bool Input::getMousePressed(short button)
{
	return getMousePressed(VK_LBUTTON + button);
}

void Input::setClientCursorPos(int x, int y)
{
	POINT p = {x, y};
	ClientToScreen(wnd->getHWND(), &p);
	setScreenCursorPos(p.x, p.y);
}

void Input::setScreenCursorPos(int x, int y)
{
	SetCursorPos(x, y);
}

int Input::getX()
{
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(wnd->getHWND(), &p);
	return p.x;
}

int Input::getY()
{
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(wnd->getHWND(), &p);
	return p.y;
}