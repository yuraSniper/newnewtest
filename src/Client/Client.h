#pragma once

#include <std.h>

class GlobalRenderer;
class Window;
class Input;
class World;
class EntityPlayer;
class GeneralThread;

class Client
{
	shared_ptr<Window> mainWnd;
	shared_ptr<Input> input;
	shared_ptr<GlobalRenderer> renderer;

	Client();
	Client(const Client &);
	Client operator=(const Client &);

	bool working;
	static void onClose(void *, void *);

	void handleInput();
	void mainLoop();
public:

	shared_ptr<World> world;
	shared_ptr<EntityPlayer> player;
	shared_ptr<GeneralThread> worldLoaderThread;

	static Client & getInstance();
	shared_ptr<Window> getMainWindow();
	shared_ptr<GlobalRenderer> getGlobalRenderer();

	void shutdown();
};