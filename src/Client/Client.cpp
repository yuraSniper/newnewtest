#include "Client.h"
#include <Window\Window.h>
#include <Window\Input.h>
#include <Render\Client\GlobalRenderer.h>
#include <Game\World\World.h>
#include <Game\Entity\EntityPlayer.h>
#include <Util\Util.h>
#include <Render\Client\TileRenderer.h>
#include <Thread\GeneralThread.h>
#include <Game\Chunk\Chunk.h>

#include <Game\Tile\Tile.h>

void Client::onClose(void * param, void *)
{
	((Client *)param)->shutdown();
}

Client & Client::getInstance()
{
	static Client instance;
	return instance;
}

Client::Client()
{
	mainWnd = make_shared<Window>(800, 600);
	mainWnd->setTitle("newNewTest");

	renderer = make_shared<GlobalRenderer>();

	worldLoaderThread = make_shared<GeneralThread>();
	worldLoaderThread->resume();

	input = make_shared<Input>(mainWnd);
	mainWnd->onCLose.add(onClose, this);

	world = make_shared<World>();
	player = make_shared<EntityPlayer>();
	player->relPos.x = 8;
	player->relPos.z = 8;
	player->chunkPos.x = 0;
	player->chunkPos.z = 0;
	player->relPos.y = 64;
	player->angles.x = -89;
	

	// TODO : TEMPORARY
	new Tile("testTile");
	renderer->initRenderers();
	renderer->sortRenderers(player);
	renderer->updateRenderersPosition(player);

	renderer->init();
	mainWnd->setVisibility(true);

	working = true;
	mainLoop();
}

void Client::mainLoop()
{
	Timer t;
	while (working)
	{
		t.start();
		mainWnd->messageLoop();
		handleInput();
		while (t.diff() < 10)
			Sleep(1);
	}
	renderer->finalize();
	worldLoaderThread->finalize();
}

void Client::handleInput()
{
	static int prevX = input->getX(), prevY = input->getY();
	static const float k = 0.4;
	static CriticalSection crit = renderer->getRenderThread().lock()->crit;
	int x = input->getX(), y = input->getY();

	glm::vec3 pos = player->relPos;
	glm::vec3 angles = player->angles;

	if (input->getKey('W'))
	{
		pos.x -= sin(Util::deg2rad(angles.y)) * k;
		pos.z -= cos(Util::deg2rad(angles.y)) * k;
	}

	if (input->getKey('S'))
	{
		pos.x += sin(Util::deg2rad(angles.y)) * k;
		pos.z += cos(Util::deg2rad(angles.y)) * k;
	}

	if (input->getKey('A'))
	{
		pos.x -= sin(Util::deg2rad(angles.y + 90)) * k * 0.6;
		pos.z -= cos(Util::deg2rad(angles.y + 90)) * k * 0.6;
	}

	if (input->getKey('D'))
	{
		pos.x += sin(Util::deg2rad(angles.y + 90)) * k * 0.6;
		pos.z += cos(Util::deg2rad(angles.y + 90)) * k * 0.6;
	}

	if (input->getKey(VK_LSHIFT))
	{
		pos.y -= k;
	}

	if (input->getKey(VK_SPACE))
	{
		pos.y += k;
	}

	if (input->getKeyPressed(VK_F4))
	{
		mainWnd->setCursorVisibility(!mainWnd->isCursorVisible());
		prevX = x;
		prevY = y;
	}

	if (!mainWnd->isCursorVisible())
	{
		int dx = x - prevX;
		int dy = y - prevY;

		RECT r;
		POINT p = {0};
		ClientToScreen(mainWnd->getHWND(), &p);
		r.left = p.x;
		r.top = p.y;
		p.x = mainWnd->getWidth();
		p.y = mainWnd->getHeight();
		ClientToScreen(mainWnd->getHWND(), &p);
		r.right = p.x;
		r.bottom = p.y;
		ClipCursor(&r);

		if (x < 10 || x > mainWnd->getWidth() - 10 || y < 10 || y > mainWnd->getHeight() - 10)
		{
			x = prevX = mainWnd->getWidth() / 2;
			y = prevY = mainWnd->getHeight() / 2;
			input->setClientCursorPos(x, y);
		}

		if (dx != 0 || dy != 0)
		{
			prevX = x;
			prevY = y;

			angles.x -= dy * 0.5;
			angles.y -= dx * 0.5;

			if (angles.y >= 360)
				angles.y -= 360;
			if (angles.y < 0)
				angles.y += 360;
			if (angles.x > 89)
				angles.x = 89;
			if (angles.x < -89)
				angles.x = -89;
		}
	}
	else
		ClipCursor(nullptr);

	crit.lock();
	player->angles = angles;

	pos.x -= pos.x < 0 ? Chunk::size : 0;
	pos.z -= pos.z < 0 ? Chunk::size : 0;

	player->chunkPos.x += ((int) pos.x) / Chunk::size;
	pos.x = fmod(pos.x, Chunk::size);
	pos.x += pos.x < 0 ? Chunk::size : 0;

	player->chunkPos.z += ((int) pos.z) / Chunk::size;
	pos.z = fmod(pos.z, Chunk::size);
	pos.z += pos.z < 0 ? Chunk::size : 0;

	player->relPos = pos;
	crit.unlock();
}

void Client::shutdown()
{
	working = false;
}

shared_ptr<GlobalRenderer> Client::getGlobalRenderer()
{
	return renderer;
}

shared_ptr<Window> Client::getMainWindow()
{
	return mainWnd;
}