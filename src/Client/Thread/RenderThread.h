#pragma once

#include <std.h>
#include <Thread\Thread.h>
#include <Client\Client.h>
#include <Render\Client\GlobalRenderer.h>

class RenderThread : public Thread
{
	bool working = true;
public:
	void run()
	{
		GlobalRenderer & renderer = *Client::getInstance().getGlobalRenderer();
		renderer.initGL();

		while (working)
		{
			renderer.render();
		}
	}

	void finalize()
	{
		working = false;
	}
};