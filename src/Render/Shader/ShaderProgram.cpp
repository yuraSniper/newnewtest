#include "ShaderProgram.h"

ShaderProgram::ShaderProgram()
{
	id = -1;
}

ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(id);
}

shared_ptr<ShaderProgram> ShaderProgram::crateFromFile(string fileName)
{
	ShaderProgram * program = new ShaderProgram();

	program->file = fileName;
	program->reload();

	return shared_ptr<ShaderProgram>(program);
}

bool ShaderProgram::reload()
{
	ifstream f;
	f.open(file, ios::in);
	string source;
	source.assign(istreambuf_iterator<char>(f), istreambuf_iterator<char>());
	f.close();

	unsigned int id = glCreateProgram();

	vector<int> shaderMarkers;
	vector<int> shaders;

	int successful = true;

	for (unsigned int i = 0; i < shaderTags.size(); i++)
	{
		string shaderTag = get<0>(shaderTags[i]);

		shaderMarkers.push_back(source.find(shaderTag));

		if (shaderMarkers[i] > 2)
		{
			string tmp = source.substr(shaderMarkers[i] - 2, 2);
			if (tmp == "/*" || tmp == "//")
			{
				source.replace(shaderMarkers[i] - 2, 1, 1, '\0');
				shaderMarkers[i] = -1;
			}
		}

		if (shaderMarkers[i] != -1)
			source.replace(shaderMarkers[i], shaderTag.length(), shaderTag.length(), '\0');
	}

	for (unsigned int i = 0; i < shaderTags.size(); i++)
	{
		if (shaderMarkers[i] != -1)
		{
			shaders.push_back(glCreateShader(get<1>(shaderTags[i])));

			unsigned int shader = shaders.back();

			string substr = source.substr(shaderMarkers[i] + get<0>(shaderTags[i]).length());
			const char * str = substr.c_str();

			glShaderSource(shader, 1, &str, nullptr);

			glCompileShader(shader);

			int status = GL_FALSE;
			glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

			if (status != GL_TRUE)
			{
				int len = 0;
				glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);

				char * log = new char[len];
				glGetShaderInfoLog(shader, len, nullptr, log);

				cout << "Shader " << get<0>(shaderTags[i]) << " " << shader << " compilation errors : " << endl;

				cout << log << endl;
				delete log;

				successful = false;
			}
		}
	}

	if (!successful)
	{
		cout << "Some of the shaders in program " << id << " has compilation errors. Program isn't loaded." << endl;
		glDeleteProgram(id);
		return false;
	}

	for (int shader : shaders)
		glAttachShader(id, shader);

	glLinkProgram(id);

	for (int shader : shaders)
	{
		glDetachShader(id, shader);
		glDeleteShader(shader);
	}

	shaders.clear();

	int status = GL_FALSE;
	glGetProgramiv(id, GL_LINK_STATUS, &status);

	if (status != GL_TRUE)
	{
		int len = 0;
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);

		char * log = new char[len];
		glGetProgramInfoLog(id, len, nullptr, log);

		cout << "Program " << id << " linking errors : " << endl;
		cout << log << endl;

		delete log;

		cout << "Program " << id << " has errors. Program isn't loaded." << endl;
		glDeleteProgram(id);

		return false;
	}

	if (this->id != -1)
		glDeleteProgram(this->id);
	this->id = id;

	return true;
}

void ShaderProgram::bind()
{
	glUseProgram(id);
}

unsigned int ShaderProgram::getUniformLocation(string uniformname)
{
	return glGetUniformLocation(id, uniformname.c_str());
}