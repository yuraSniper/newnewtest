#pragma once

#include <std.h>

const vector<tuple<string, int>> shaderTags = {
	make_tuple("#Vertex", GL_VERTEX_SHADER),
	make_tuple("#TessControl", GL_TESS_CONTROL_SHADER),
	make_tuple("#TessEval", GL_TESS_EVALUATION_SHADER),
	make_tuple("#Geometry", GL_GEOMETRY_SHADER),
	make_tuple("#Fragment", GL_FRAGMENT_SHADER)};

class ShaderProgram
{
	unsigned int id = 0;
	string file;

	ShaderProgram();
public:
	~ShaderProgram();

	static shared_ptr<ShaderProgram> crateFromFile(string fileName);
	bool reload();
	void bind();

	unsigned int getUniformLocation(string uniformname);
};