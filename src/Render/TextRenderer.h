#pragma once

#include <std.h>
#include <Render\Resource\Font.h>

class ShaderProgram;

class TextRenderer
{
	struct Glyph
	{
		float x;
		float r, g, b, a;
		char c;
	};

	unsigned int vao, vbo;
	Font font;
	shared_ptr<ShaderProgram> fontProgram;

	TextRenderer();
	TextRenderer(const TextRenderer &);
	TextRenderer & operator=(TextRenderer &);

public:
	static TextRenderer & getInstance();

	void renderString(string str);
	void renderStringFormat(string format, ...);
};