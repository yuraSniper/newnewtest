#pragma once

#include <std.h>

class RenderBuffer;
class Entity;
class GlobalRenderer;
class ChunkCache;
class World;

class ChunkRenderer
{
	shared_ptr<ChunkCache> cache;

	shared_ptr<World> world;

	shared_ptr<RenderBuffer> mesh;

	bool checkNeighborChunks();
public:
	ChunkRenderer(shared_ptr<World> world, short xPos, short yPos, short zPos);

	short xPos, yPos, zPos;
	bool needUpdate;

	void setPosition(short x, short y, short z);

	void update();
	void render();

	double distanceSq(shared_ptr<Entity> e);
};