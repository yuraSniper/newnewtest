#pragma once

struct Vertex
{
	float x, y, z;
	float u, v;
	int color;
	char atlas, texture;
	Vertex(float x = 0, float y = 0, float z = 0, float u = 0, float v = 0, char atlas = 0, char texture = 0, int color = 0xFFFFFFFF)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->u = u;
		this->v = v;
		this->atlas = atlas;
		this->texture = texture;
		this->color = color;
	}
};