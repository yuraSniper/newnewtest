#include "ChunkRenderer.h"

#include <Client\Client.h>
#include <Render\Client\RenderBuffer.h>

#include <Render\Client\TileRenderer.h>
#include <Render\Client\GlobalRenderer.h>

#include <Game\Chunk\ChunkCache.h>
#include <Game\Chunk\Chunk.h>
#include <Game\Entity\EntityPlayer.h>
#include <Game\World\World.h>

#include <Thread\Task\LoadChunkTask.h>
#include <Thread\GeneralThread.h>

#include <Util\Util.h>

ChunkRenderer::ChunkRenderer(shared_ptr<World> world, short xPos, short yPos, short zPos)
{
	this->world = world;
	this->xPos = xPos;
	this->yPos = yPos;
	this->zPos = zPos;

	needUpdate = true;

	mesh = make_shared<RenderBuffer>();
}

void ChunkRenderer::update()
{
	if (needUpdate && world != nullptr)
	{
		TileRenderer::buffer = mesh;
		cache = make_shared<ChunkCache>(world, xPos, yPos, zPos);
		if (checkNeighborChunks())
		{
			needUpdate = false;
			for (int x = 0; x < Chunk::size; x++)
			{
				int xx = x + xPos * Chunk::size;
				for (int z = 0; z < Chunk::size; z++)
				{
					int zz = z + zPos * Chunk::size;
					for (int y = 0; y < Chunk::size; y++)
					{
						int yy = y + yPos * Chunk::size;

						mesh->setOffset(x, y, z);
						TileRenderer::renderFullBlock(cache.get(), xx, yy, zz);
					}
				}
			}
		}
	}
}

void ChunkRenderer::setPosition(short x, short y, short z)
{
	if (xPos != x || yPos != y || zPos != z)
	{
		xPos = x;
		yPos = y;
		zPos = z;
		needUpdate = true;
		mesh->clear();
	}
}

void ChunkRenderer::render()
{
	mesh->draw();
}

double ChunkRenderer::distanceSq(shared_ptr<Entity> e)
{
	return sqrt(Util::sqr(e->chunkPos.x - xPos) +
		Util::sqr(e->relPos.y - yPos * Chunk::size) +
		Util::sqr(e->chunkPos.z - zPos));
}

bool ChunkRenderer::checkNeighborChunks()
{
	bool b = true;
	
	for (short x = -1; x < 2; x++)
		for (short z = -1; z < 2; z++)
			if (cache->getChunk(x + xPos, z + zPos) == nullptr)
			{
				Client::getInstance().worldLoaderThread->scheduleTask(
					new LoadChunkTask(world, xPos + x, zPos + z));
				b = false;
			}

	return b;
}