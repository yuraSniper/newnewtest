#include "TileRenderer.h"

#include <Client\Client.h>

#include <Game\Registry\TileRegistry.h>
#include <Game\Tile\Tile.h>
#include <Game\World\IWorld.h>

#include <Render\Client\GlobalRenderer.h>
#include <Render\Client\RenderBuffer.h>
#include <Render\Client\Vertex.h>

shared_ptr<RenderBuffer> TileRenderer::buffer;

void TileRenderer::renderFullBlock(IWorld * wld, int x, int y, int z)
{
	Tile * tile = TileRegistry::getInstance().getTileById(wld->getBlockId(x, y, z));

	if (tile == nullptr)
		return;

	static Vertex top[] = {
		Vertex(0, 1, 0, 0, 0, 0, 0, 0xff007fff),
		Vertex(0, 1, 1, 0, 1, 0, 0, 0xff007fff),
		Vertex(1, 1, 1, 1, 1, 0, 0, 0xff007fff),
		Vertex(1, 1, 0, 1, 0, 0, 0, 0xff007fff)
	};
	static Vertex bottom[] = {
		Vertex(0, 0, 1, 0, 0, 0, 0, 0xff007fff),
		Vertex(0, 0, 0, 0, 1, 0, 0, 0xff007fff),
		Vertex(1, 0, 0, 1, 1, 0, 0, 0xff007fff),
		Vertex(1, 0, 1, 1, 0, 0, 0, 0xff007fff)
	};
	static Vertex north[] = {
		Vertex(1, 1, 0, 0, 0, 0, 0, 0xff00ff00),
		Vertex(1, 0, 0, 0, 1, 0, 0, 0xff00ff00),
		Vertex(0, 0, 0, 1, 1, 0, 0, 0xff00ff00),
		Vertex(0, 1, 0, 1, 0, 0, 0, 0xff00ff00)
	};
	static Vertex west[] = {
		Vertex(0, 1, 0, 0, 0, 0, 0, 0xff00ffff),
		Vertex(0, 0, 0, 0, 1, 0, 0, 0xff00ffff),
		Vertex(0, 0, 1, 1, 1, 0, 0, 0xff00ffff),
		Vertex(0, 1, 1, 1, 0, 0, 0, 0xff00ffff)
	};
	static Vertex south[] = {
		Vertex(0, 1, 1, 0, 0, 0, 0, 0xff00ff00),
		Vertex(0, 0, 1, 0, 1, 0, 0, 0xff00ff00),
		Vertex(1, 0, 1, 1, 1, 0, 0, 0xff00ff00),
		Vertex(1, 1, 1, 1, 0, 0, 0, 0xff00ff00)
	};
	static Vertex east[] = {
		Vertex(1, 1, 1, 0, 0, 0, 0, 0xff00ffff),
		Vertex(1, 0, 1, 0, 1, 0, 0, 0xff00ffff),
		Vertex(1, 0, 0, 1, 1, 0, 0, 0xff00ffff),
		Vertex(1, 1, 0, 1, 0, 0, 0, 0xff00ffff)
	};

	if (tile->shouldRenderFace(wld, x, y + 1, z, TileFace::TOP))
		buffer->addVertices(top, 4);

	if (tile->shouldRenderFace(wld, x, y - 1, z, TileFace::BOTTOM))
		buffer->addVertices(bottom, 4);

	if (tile->shouldRenderFace(wld, x, y, z - 1, TileFace::NORTH))
		buffer->addVertices(north, 4);

	if (tile->shouldRenderFace(wld, x - 1, y, z, TileFace::WEST))
		buffer->addVertices(west, 4);

	if (tile->shouldRenderFace(wld, x, y, z + 1, TileFace::SOUTH))
		buffer->addVertices(south, 4);

	if (tile->shouldRenderFace(wld, x + 1, y, z, TileFace::EAST))
		buffer->addVertices(east, 4);
}