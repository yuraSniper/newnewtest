#include "RenderBuffer.h"

RenderBuffer::RenderBuffer()
{
	vbo = 0;
	updated = false;
	size = 0;
	offsetX = offsetY = offsetZ = 0;
}

RenderBuffer::~RenderBuffer()
{
	glDeleteBuffers(1, &vbo);
}

void RenderBuffer::addVertex(Vertex & v)
{
	buffer.push_back(Vertex(v.x + offsetX, v.y + offsetY, v.z + offsetZ, v.u, v.v, v.atlas, v.texture, v.color));
	size++;
	updated = true;
}

void RenderBuffer::addVertices(Vertex * verts, int count)
{
	for (int i = 0; i < count; i++)
		buffer.push_back(Vertex(verts[i].x + offsetX, verts[i].y + offsetY, verts[i].z + offsetZ, verts[i].u, verts[i].v, verts[i].atlas, verts[i].texture, verts[i].color));
	size += count;
	updated = true;
}

void RenderBuffer::draw()
{
	if (size == 0)
		return;

	if (vbo == 0)
		glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	if (updated)
	{
		size = buffer.size();
		glBufferData(GL_ARRAY_BUFFER, size * sizeof(Vertex), &buffer[0], GL_STREAM_DRAW);
		buffer.clear();
		buffer.shrink_to_fit();
		updated = false;
	}

	//glEnableClientState(GL_VERTEX_ARRAY);
	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	//glEnableClientState(GL_COLOR_ARRAY);
	//glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), (void *) offsetof(Vertex, x));
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, true, sizeof(Vertex), (void *) offsetof(Vertex, color));
	glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(Vertex), (void *) offsetof(Vertex, u));
	glVertexAttribIPointer(3, 2, GL_UNSIGNED_BYTE, sizeof(Vertex), (void *) offsetof(Vertex, atlas));
	//glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), (void *)offsetof(Vertex, u));
	//glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Vertex), (void *)offsetof(Vertex, color));
	//glVertexAttribIPointer(1, 2, GL_UNSIGNED_BYTE, sizeof(Vertex), (void *)offsetof(Vertex, atlas));

	glDrawArrays(GL_QUADS, 0, size);

	//glDisableClientState(GL_VERTEX_ARRAY);
	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	//glDisableClientState(GL_COLOR_ARRAY);
	//glDisableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void RenderBuffer::clear()
{
	updated = true;
	size = 0;
	buffer.clear();
}

void RenderBuffer::setOffset(float x, float y, float z)
{
	offsetX = x;
	offsetY = y;
	offsetZ = z;
}

void RenderBuffer::addOffset(float x, float y, float z)
{
	offsetX += x;
	offsetY += y;
	offsetZ += z;
}