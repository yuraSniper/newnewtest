#pragma once

#include <std.h>
#include <Game\Tile\TileFace.h>

class RenderBuffer;
class IWorld;

class TileRenderer
{
public:
	static shared_ptr<RenderBuffer> buffer;

	static void renderFullBlock(IWorld * wld, int x, int y, int z);
};