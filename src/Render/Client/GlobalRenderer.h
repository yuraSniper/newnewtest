#pragma once

#include <std.h>
#include <glm.h>

#include <Render\Resource\Texture.h>

class Client;
class IWorld;
class Thread;
class Window;
class RenderBuffer;
class ChunkRenderer;
class Entity;
class ShaderProgram;

class GlobalRenderer
{
	HGLRC hRC;
	Client * client;
	shared_ptr<Window> wnd;
	shared_ptr<Thread> renderThread;

	shared_ptr<ShaderProgram> program;
	unsigned int fbo, fboColorTex, fboDepthTex;
	unsigned int worldRenderVAO;
	Texture texture;

	vector<ChunkRenderer *> renderers;
	vector<ChunkRenderer *> sortedRenderers;

	const int renderRadius = 5, renderSize = 2 * renderRadius - 1;
	int prevWidth, prevHeight;

	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 projection;

public:
	bool skipWorldRendering;

	GlobalRenderer();

	void initRenderers();
	void sortRenderers(shared_ptr<Entity> e);
	void updateRenderers(int count = 1);
	void markTilesForUpdate(int x0, int y0, int z0, int x1, int y1, int z1);
	void markTileForUpdate(int x, int y, int z);
	void updateRenderersPosition(shared_ptr<Entity> e);

	weak_ptr<Thread> getRenderThread();

	void init();
	void finalize();
	void initGL();
	void render();

	glm::mat4 getMVP();
};