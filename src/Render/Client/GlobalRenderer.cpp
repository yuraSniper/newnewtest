#include "GlobalRenderer.h"

#include <Client\Client.h>
#include <Client\Thread\RenderThread.h>

#include <Game\Chunk\Chunk.h>
#include <Game\Entity\EntityPlayer.h>

#include <Render\Client\ChunkRenderer.h>
#include <Render\Client\RenderBuffer.h>
#include <Render\Client\TileRenderer.h>
#include <Render\Resource\ResourceManager.h>
#include <Render\Shader\ShaderProgram.h>
#include <Render\TextRenderer.h>

#include <Util\Util.h>
#include <Window\Window.h>

GlobalRenderer::GlobalRenderer()
{
	client = &Client::getInstance();
	wnd = client->getMainWindow();
	hRC = nullptr;

	renderThread = make_shared<RenderThread>();

	skipWorldRendering = false;
}

void GlobalRenderer::initRenderers()
{
	if (!renderers.empty())
	{
		for (int i = 0; i < renderers.size(); i++)
			delete renderers[i];
		renderers.clear();
		sortedRenderers.clear();
		renderers.shrink_to_fit();
		sortedRenderers.shrink_to_fit();
	}

	shared_ptr<World> wld = client->world;

	renderers.reserve(Util::sqr(renderSize) * (Chunk::height / Chunk::size));
	sortedRenderers.reserve(renderers.capacity());

	for (int x = 0; x < renderSize; x++)
		for (int z = 0; z < renderSize; z++)
			for (int y = 0; y < (Chunk::height / Chunk::size); y++)
			{
				renderers.push_back(new ChunkRenderer(wld, x - renderRadius + 1, y, z - renderRadius + 1));
				sortedRenderers.push_back(renderers[renderers.size() - 1]);
			}
}

void GlobalRenderer::sortRenderers(shared_ptr<Entity> e)
{
	sort(sortedRenderers.begin(), sortedRenderers.end(), [&e] (ChunkRenderer * a, ChunkRenderer * b)
	{
		return a->distanceSq(e) < b->distanceSq(e);
	});
}

void GlobalRenderer::updateRenderers(int count)
{
	for (int i = 0; count > 0 && i < sortedRenderers.size(); i++)
	{
		if (sortedRenderers[i]->needUpdate)
		{
			sortedRenderers[i]->update();
			count--;
		}
	}
}

void GlobalRenderer::markTilesForUpdate(int x0, int y0, int z0, int x1, int y1, int z1)
{
	int maxx = renderers[renderers.size() - 1]->xPos;
	int maxy = renderers[renderers.size() - 1]->yPos;
	int maxz = renderers[renderers.size() - 1]->zPos;
	for (int x = max(x0 / Chunk::size, (int)renderers[0]->xPos); x <= min(x1 / Chunk::size, maxx); x++)
	{
		int xx = x - renderers[0]->xPos;
		for (int z = max(z0 / Chunk::size, (int)renderers[0]->zPos); z <= min(z1 / Chunk::size, maxz); z++)
		{
			int zz = z - renderers[0]->zPos;
			for (int y = max(y0 / Chunk::size, (int)renderers[0]->yPos); y <= min(y1 / Chunk::size, maxy); y++)
			{
				int yy = y - renderers[0]->yPos;
				renderers[(xx * renderSize + zz) * (Chunk::height / Chunk::size) + yy]->needUpdate = true;
			}
		}
	}
}

void GlobalRenderer::updateRenderersPosition(shared_ptr<Entity> e)
{
	static int prevx = 0;
	static int prevz = 0;

	int dx = prevx - e->chunkPos.x;
	int dz = prevz - e->chunkPos.z;

	if (dx == 0 && dz == 0)
		return;

	prevx = e->chunkPos.x;
	prevz = e->chunkPos.z;

	for (int i = 0; i < renderers.size(); i++)
	{
		bool b = false;
		ChunkRenderer * renderer = renderers[i];
		int tmpx = renderers[i]->xPos;
		int tmpz = renderers[i]->zPos;

		if (tmpx + dx < prevx - renderRadius)
		{
			tmpx += renderSize;
			b = true;
		}
		if (tmpz + dz < prevz - renderRadius)
		{
			tmpz += renderSize;
			b = true;
		}
		if (tmpx + dx > prevx + renderRadius)
		{
			tmpx -= renderSize;
			b = true;
		}
		if (tmpz + dz > prevz + renderRadius)
		{
			tmpz -= renderSize;
			b = true;
		}

		if (b)
			renderer->setPosition(tmpx, renderer->yPos, tmpz);
	}

	sortRenderers(e);
}

void GlobalRenderer::markTileForUpdate(int x, int y, int z)
{
	markTilesForUpdate(x - 1, y - 1, z - 1, x + 1, y + 1, z + 1);
}

weak_ptr<Thread> GlobalRenderer::getRenderThread()
{
	return renderThread;
}

void GlobalRenderer::init()
{
	renderThread->resume();
}

void GlobalRenderer::finalize()
{
	renderThread->finalize();
}

void GlobalRenderer::initGL()
{
	hRC = wglCreateContext(wnd->getHDC());
	wglMakeCurrent(wnd->getHDC(), hRC);

	glewInit();

	glClearColor(0, 0, 0, 0);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1);

	glEnable(GL_CULL_FACE);

	glGenTextures(1, &fboColorTex);
	glGenTextures(1, &fboDepthTex);
	glGenFramebuffers(1, &fbo);

	glGenVertexArrays(1, &worldRenderVAO);
	glBindVertexArray(worldRenderVAO);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);

	ResourceManager & manager = ResourceManager::getInstance();
	TextRenderer::getInstance();

	program = ShaderProgram::crateFromFile("data\\Shaders\\FlatTextureAtlas.glsl");
	texture = manager.loadTexture("data\\Textures\\Test.png");

	TextureAtlas atlas = manager.getAtlasInfo(texture.getAtlasId());
	atlas.bind(2);
	
	program->bind();

	int texUnits[] = {2};
	int rowCounts[] = {atlas.getTexturesInRow()};
	glUniform1iv(program->getUniformLocation("tex"), 1, texUnits);
	glUniform1iv(program->getUniformLocation("texRowCount"), 1, rowCounts);

	glUseProgram(0);
}

void GlobalRenderer::render()
{
	if (hRC == nullptr)
		return;

	int width = wnd->getWidth(), height = max(wnd->getHeight(), 1);

	if (width == 0)
		return;

	Timer t;
	t.start();
	string title = "newNewTest";

	shared_ptr<World> wld = client->world;
	shared_ptr<EntityPlayer> player = client->player;

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	if (prevWidth != width || prevHeight != height)
	{
		prevWidth = width;
		prevHeight = height;

		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, fboColorTex);
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGB, width, height, false);
		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, fboDepthTex);
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_DEPTH_COMPONENT32, width, height, false);
		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, fboColorTex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, fboDepthTex, 0);

		glViewport(0, 0, width, height);
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	model = glm::mat4();
	view = glm::mat4();

	if (!skipWorldRendering && wld != nullptr && player != nullptr)
	{
		projection = glm::perspectiveFov<float>(45, width, height, 0.1f, 1000);

		renderThread->crit.lock();

		updateRenderersPosition(player);

		view = glm::rotate(view, -player->angles.x, glm::vec3(1, 0, 0));
		view = glm::rotate(view, -player->angles.y, glm::vec3(0, 1, 0));
		view = glm::rotate(view, -player->angles.z, glm::vec3(0, 0, 1));
		view = glm::translate(view, -player->relPos - player->eyeOffset);

		glm::ivec3 playerPos = player->chunkPos;
		renderThread->crit.unlock();

		glm::ivec3 pos;
		ChunkRenderer * renderer;

		program->bind();

		glBindVertexArray(worldRenderVAO);

		for (int i = 0; i < sortedRenderers.size(); i++)
		{
			renderer = sortedRenderers[i];

			pos = glm::ivec3(renderer->xPos, renderer->yPos, renderer->zPos);
			model = glm::translate(glm::mat4(), glm::vec3((pos - playerPos) * Chunk::size));

			glUniformMatrix4fv(program->getUniformLocation("mvp"), 1, false, glm::value_ptr(getMVP()));

			sortedRenderers[i]->render();
		}

		glBindVertexArray(0);
		glUseProgram(0);

		updateRenderers();
	}

	projection = glm::ortho<float>(0, width, height, 0);
	view = glm::mat4();
	
	float dt = t.diff(1000000) * 0.000001;

	model = glm::scale(glm::mat4(), glm::vec3(12, 14, 1));

	TextRenderer::getInstance().renderStringFormat("dt : %10.8f relX : %3.1f  relY : %3.1f  relZ : %3.1f chunkX : %i  chunkZ : %i",
		dt, player->relPos.x, player->relPos.y, player->relPos.z, player->chunkPos.x, player->chunkPos.z);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
	glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

	wnd->swapBuffers();
}

glm::mat4 GlobalRenderer::getMVP()
{
	return projection * view * model;
}