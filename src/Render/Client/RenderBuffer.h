#pragma once

#include <std.h>
#include <Render\Client\Vertex.h>

class RenderBuffer
{
	unsigned int vbo;
	vector<Vertex> buffer;
	bool updated;
	int size;

	float offsetX, offsetY, offsetZ;
public:
	RenderBuffer();
	~RenderBuffer();

	void setOffset(float x, float y, float z);
	void addOffset(float x, float y, float z);

	void addVertex(Vertex & v);
	void addVertices(Vertex * verts, int count);

	void draw();
	void clear();
};