#pragma once

#include <std.h>

class Texture
{
	int textureAtlas, slot;
	int id;

public:
	Texture(int textureAtlas = -1, int id = -1, int slot = 0);

	int getId();
	int getAtlasId();
	int getAtlasSlot();
};