#include "Texture.h"

Texture::Texture(int textureAtlas, int id, int slot)
{
	this->textureAtlas = textureAtlas;
	this->id = id;
	this->slot = slot;
}

int Texture::getId()
{
	return id;
}

int Texture::getAtlasId()
{
	return textureAtlas;
}

int Texture::getAtlasSlot()
{
	return slot;
}