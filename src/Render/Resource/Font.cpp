#include "Font.h"

Font::Font(unsigned int id, int size) : TextureAtlas(id, size, size / 16)
{
}

int Font::getTexturesInRow()
{
	return 16;
}

int Font::getFreeSlots()
{
	return 0;
}

bool Font::decrementFreeSlots()
{
	return false;
}