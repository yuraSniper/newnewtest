#include "TextRenderer.h"

#include <Render\Resource\ResourceManager.h>
#include <Render\Shader\ShaderProgram.h>

#include <Client\Client.h>
#include <Render\Client\GlobalRenderer.h>

#include <glm.h>

TextRenderer & TextRenderer::getInstance()
{
	static TextRenderer instance;
	return instance;
}

TextRenderer::TextRenderer()
{
	int prevProgram, prevVAO;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prevProgram);
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &prevVAO);

	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(0, 1, GL_FLOAT, false, sizeof(Glyph), (void *) offsetof(Glyph, x));
	glVertexAttribPointer(1, 4, GL_FLOAT, false, sizeof(Glyph), (void *) offsetof(Glyph, r));
	glVertexAttribIPointer(2, 1, GL_UNSIGNED_BYTE, sizeof(Glyph), (void *) offsetof(Glyph, c));

	glBindVertexArray(prevVAO);

	font = ResourceManager::getInstance().loadFont("data\\Textures\\Font.png");
	fontProgram = ShaderProgram::crateFromFile("data\\Shaders\\Font.glsl");

	fontProgram->bind();
	glUniform1i(fontProgram->getUniformLocation("tex"), 1);

	glUseProgram(prevProgram);

	font.bind(1);
}

void TextRenderer::renderString(string str)
{
	int prevProgram, prevVAO;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prevProgram);
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &prevVAO);

	fontProgram->bind();

	GlobalRenderer & renderer = *Client::getInstance().getGlobalRenderer().get();
	glm::mat4 mvp = renderer.getMVP();

	glUniformMatrix4fv(fontProgram->getUniformLocation("mvp"), 1, false, glm::value_ptr(mvp));

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	vector<Glyph> data;

	for (int i = 0; i < str.length(); i++)
		data.push_back({i, 1, 1, 1, 1, str.at(i) - 32});

	glBufferData(GL_ARRAY_BUFFER, sizeof(Glyph) * data.size(), &(data[0]), GL_STREAM_DRAW);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDrawArrays(GL_POINTS, 0, str.length());

	glDisable(GL_BLEND);

	glBindVertexArray(prevVAO);
	glUseProgram(prevProgram);
}

void TextRenderer::renderStringFormat(string format, ...)
{
	char str[256];
	va_list list;

	va_start(list, format);
	vsprintf(str, format.c_str(), list);
	va_end(list);

	renderString(string(str));
}