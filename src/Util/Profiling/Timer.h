#pragma once

#include <std.h>

class Timer
{
	LARGE_INTEGER freq;
	LARGE_INTEGER startPoint;
public:
	Timer();
	void start();
	long long diff(int mult = 1000);
};