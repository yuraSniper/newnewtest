#pragma once

template <typename T>
struct Vec3;

typedef Vec3<int> Vec3i;
typedef Vec3<double> Vec3d;

template <typename T>
struct Vec3
{
	T x, y, z;

	Vec3(T x, T y, T z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vec3()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	Vec3 & operator =(Vec3 & v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}

	Vec3 operator +(Vec3 & v)
	{
		return Vec3(x + v.x, y + v.y, z + v.z);
	}

	Vec3 & operator +=(Vec3 & v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	Vec3 operator -()
	{
		return Vec3(-x, -y, -z);
	}

	Vec3 operator -(Vec3 & v)
	{
		return *this + (-v);
	}

	Vec3 & operator -=(Vec3 & v)
	{
		return *this += -v;
	}

	Vec3 operator *(T k)
	{
		return Vec3(x * k, y * k, z * k);
	}

	Vec3 & operator *=(T k)
	{
		this->x *= k;
		this->y *= k;
		this->z *= k;
		return *this;
	}

	double dot(Vec3 & v)
	{
		return x * v.x + y * v.y + z * v.z;
	}

	Vec3 cross(Vec3 & v)
	{
		return Vec3(y * v.z - z * v.y, - x * v.z + z * v.x, x * v.y - y * v.x);
	}

	double len()
	{
		return sqrt(lenSqr());
	}

	double lenSqr()
	{
		return x * x + y * y + z * z;
	}
};