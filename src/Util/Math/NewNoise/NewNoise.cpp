#include "NewNoise.h"

double NewNoise::raw(double x, double y)
{
	int xx = floor(x);
	int yy = floor(y);

	x -= xx;
	y -= yy;

	xx &= 255;
	yy &= 255;

	double a = x, b = y;
	int x0 = 1, y0 = 0;

	if (x < y)
	{
		a = y;
		b = x;
		x0 = 0;
		y0 = 1;
	}

	double l, l1, l2, l3;

	l1 = fade(1 - a);
	l2 = fade(a - b);
	l3 = fade(b);

	l = l1 * grad(permTable[xx + permTable[yy]], x, y);
	l += l2 * grad(permTable[xx + permTable[yy + y0] + x0], x - x0, y - y0);
	l += l3 * grad(permTable[xx + permTable[yy + 1] + 1], x - 1, y - 1);

	return l / (l1 + l2 + l3);
}

double NewNoise::raw(double x, double y, double z)
{
	int xx = floor(x);
	int yy = floor(y);
	int zz = floor(z);

	x -= xx;
	y -= yy;
	z -= zz;

	xx &= 255;
	yy &= 255;
	zz &= 255;

	char rankx = 0, ranky = 0, rankz = 0;

	if (x >= y) rankx++; else ranky++;
	if (x >= z) rankx++; else rankz++;
	if (y >= z) ranky++; else rankz++;

	double *a, *b, *c;
	a = rankx == 2 ? &x : ranky == 2 ? &y : rankz == 2 ? &z : 0;
	b = rankx == 1 ? &x : ranky == 1 ? &y : rankz == 1 ? &z : 0;
	c = rankx == 0 ? &x : ranky == 0 ? &y : rankz == 0 ? &z : 0;

	char x0 = a == &x, y0 = a == &y, z0 = a == &z;
	char x1 = x0 || b == &x, y1 = y0 || b == &y, z1 = z0 || b == &z;
	double l, l1, l2, l3, l4;

	l1 = fade(1 - *a);
	l2 = fade(*a - *b);
	l3 = fade(*b - *c);
	l4 = fade(*c);

	l = l1 * grad(permTable[xx + permTable[yy + permTable[zz]]], x, y, z);
	l += l2 * grad(permTable[xx + permTable[yy + permTable[zz + z0] + y0] + x0], x - x0, y - y0, z - z0);
	l += l3 * grad(permTable[xx + permTable[yy + permTable[zz + z1] + y1] + x1], x - x1, y - y1, z - z1);
	l += l4 * grad(permTable[xx + permTable[yy + permTable[zz + 1] + 1] + 1], x - 1, y - 1, z - 1);

	return l / (l1 + l2 + l3 + l4);
}

double NewNoise::raw(double x, double y, double z, double w)
{
	int xx = floor(x);
	int yy = floor(y);
	int zz = floor(z);
	int ww = floor(w);

	x -= xx;
	y -= yy;
	z -= zz;
	w -= ww;

	xx &= 255;
	yy &= 255;
	zz &= 255;
	ww &= 255;

	char rankx = 0, ranky = 0, rankz = 0, rankw = 0;

	if (x >= y) rankx++; else ranky++;
	if (x >= z) rankx++; else rankz++;
	if (x >= w) rankx++; else rankw++;
	if (y >= z) ranky++; else rankz++;
	if (y >= w) ranky++; else rankw++;
	if (z >= w) rankz++; else rankw++;

	double *a, *b, *c, *d;
	a = rankx == 3 ? &x : ranky == 3 ? &y : rankz == 3 ? &z : rankw == 3 ? &w : 0;
	b = rankx == 2 ? &x : ranky == 2 ? &y : rankz == 2 ? &z : rankw == 2 ? &w : 0;
	c = rankx == 1 ? &x : ranky == 1 ? &y : rankz == 1 ? &z : rankw == 1 ? &w : 0;
	d = rankx == 0 ? &x : ranky == 0 ? &y : rankz == 0 ? &z : rankw == 0 ? &w : 0;

	char x0 = a == &x, y0 = a == &y, z0 = a == &z, w0 = a == &w;
	char x1 = x0 || b == &x, y1 = y0 || b == &y, z1 = z0 || b == &z, w1 = w0 || b == &w;
	char x2 = x1 || c == &x, y2 = y1 || c == &y, z2 = z1 || c == &z, w2 = w1 || c == &w;
	double l, l1, l2, l3, l4, l5;

	l1 = fade(1 - *a);
	l2 = fade(*a - *b);
	l3 = fade(*b - *c);
	l4 = fade(*c - *d);
	l5 = fade(*d);

	l = l1 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww]]]], x, y, z, w);
	l += l2 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + w0] + z0] + y0] + x0], x - x0, y - y0, z - z0, w - w0);
	l += l3 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + w1] + z1] + y1] + x1], x - x1, y - y1, z - z1, w - w1);
	l += l4 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + w2] + z2] + y2] + x2], x - x2, y - y2, z - z2, w - w2);
	l += l5 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + 1] + 1] + 1] + 1], x - 1, y - 1, z - 1, w - 1);
	
	return l / (l1 + l2 + l3 + l4 + l5);
}

double NewNoise::raw(double x, double y, double z, double w, double t)
{
	int xx = floor(x);
	int yy = floor(y);
	int zz = floor(z);
	int ww = floor(w);
	int tt = floor(t);

	x -= xx;
	y -= yy;
	z -= zz;
	w -= ww;
	t -= tt;

	xx &= 255;
	yy &= 255;
	zz &= 255;
	ww &= 255;
	tt &= 255;

	char rankx = 0, ranky = 0, rankz = 0, rankw = 0, rankt = 0;

	if (x >= y) rankx++; else ranky++;
	if (x >= z) rankx++; else rankz++;
	if (x >= w) rankx++; else rankw++;
	if (x >= t) rankx++; else rankt++;
	if (y >= z) ranky++; else rankz++;
	if (y >= w) ranky++; else rankw++;
	if (y >= t) ranky++; else rankt++;
	if (z >= w) rankz++; else rankw++;
	if (z >= t) rankz++; else rankt++;
	if (w >= t) rankw++; else rankt++;

	double *a, *b, *c, *d, *e;
	a = rankx == 4 ? &x : ranky == 4 ? &y : rankz == 4 ? &z : rankw == 4 ? &w : rankt == 4 ? &t : 0;
	b = rankx == 3 ? &x : ranky == 3 ? &y : rankz == 3 ? &z : rankw == 3 ? &w : rankt == 3 ? &t : 0;
	c = rankx == 2 ? &x : ranky == 2 ? &y : rankz == 2 ? &z : rankw == 2 ? &w : rankt == 2 ? &t : 0;
	d = rankx == 1 ? &x : ranky == 1 ? &y : rankz == 1 ? &z : rankw == 1 ? &w : rankt == 1 ? &t : 0;
	e = rankx == 0 ? &x : ranky == 0 ? &y : rankz == 0 ? &z : rankw == 0 ? &w : rankt == 0 ? &t : 0;

	char x0 = a == &x, y0 = a == &y, z0 = a == &z, w0 = a == &w, t0 = a == &t;
	char x1 = x0 || b == &x, y1 = y0 || b == &y, z1 = z0 || b == &z, w1 = w0 || b == &w, t1 = t0 || b == &t;
	char x2 = x1 || c == &x, y2 = y1 || c == &y, z2 = z1 || c == &z, w2 = w1 || c == &w, t2 = t1 || c == &t;
	char x3 = x2 || d == &x, y3 = y2 || d == &y, z3 = z2 || d == &z, w3 = w2 || d == &w, t3 = t2 || d == &t;
	double l, l1, l2, l3, l4, l5, l6;

	l1 = fade(1 - *a);
	l2 = fade(*a - *b);
	l3 = fade(*b - *c);
	l4 = fade(*c - *d);
	l5 = fade(*d - *e);
	l6 = fade(*e);

	l = l1 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + permTable[tt]]]]], x, y, z, w, t);
	l += l2 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + permTable[tt + t0] + w0] + z0] + y0] + x0], x - x0, y - y0, z - z0, w - w0, t - t0);
	l += l3 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + permTable[tt + t1] + w1] + z1] + y1] + x1], x - x1, y - y1, z - z1, w - w1, t - t1);
	l += l4 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + permTable[tt + t2] + w2] + z2] + y2] + x2], x - x2, y - y2, z - z2, w - w2, t - t2);
	l += l5 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + permTable[tt + t3] + w3] + z3] + y3] + x3], x - x3, y - y3, z - z3, w - w3, t - t3);
	l += l6 * grad(permTable[xx + permTable[yy + permTable[zz + permTable[ww + permTable[tt + 1] + 1] + 1] + 1] + 1], x - 1, y - 1, z - 1, w - 1, t - 1);

	return l / (l1 + l2 + l3 + l4 + l5 + l6);
}

double NewNoise::noise(int x, int y)
{
	double freq = 1;
	double amplitude = 1, maxamp = 0;
	double total = 0;

	double xx = x * scale;
	double yy = y * scale;

	for (int i = 0; i < octaves; i++)
	{
		total += raw(xx * freq, yy * freq) * amplitude;

		maxamp += amplitude;
		freq *= 2;
		amplitude *= 0.5;
	}

	return total / maxamp;
}

double NewNoise::noise(int x, int y, int z)
{
	double freq = 1;
	double amplitude = 1, maxamp = 0;
	double total = 0;

	double xx = x * scale;
	double yy = y * scale;
	double zz = z * scale;

	for (int i = 0; i < octaves; i++)
	{
		total += raw(xx * freq, yy * freq, zz * freq) * amplitude;

		maxamp += amplitude;
		freq *= 2;
		amplitude *= 0.5;
	}

	return total / maxamp;
}

double NewNoise::noise(int x, int y, int z, int w)
{
	double freq = 1;
	double amplitude = 1, maxamp = 0;
	double total = 0;

	double xx = x * scale;
	double yy = y * scale;
	double zz = z * scale;
	double ww = w * scale;

	for (int i = 0; i < octaves; i++)
	{
		total += raw(xx * freq, yy * freq, zz * freq, ww * freq) * amplitude;

		maxamp += amplitude;
		freq *= 2;
		amplitude *= 0.5;
	}

	return total / maxamp;
}

double NewNoise::noise(int x, int y, int z, int w, int t)
{
	double freq = 1;
	double amplitude = 1, maxamp = 0;
	double total = 0;

	double xx = x * scale;
	double yy = y * scale;
	double zz = z * scale;
	double ww = w * scale;
	double tt = t * scale;

	for (int i = 0; i < octaves; i++)
	{
		total += raw(xx * freq, yy * freq, zz * freq, ww * freq, tt * freq) * amplitude;

		maxamp += amplitude;
		freq *= 2;
		amplitude *= 0.5;
	}

	return total / maxamp;
}

NewNoise::NewNoise(int octaves, double scale)
{
	this->octaves = octaves;
	this->scale = scale;

	permTable = new unsigned char[512] {151, 160, 137, 91, 90, 15,
		131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
		190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
		88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
		77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
		102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
		135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
		5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
		223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
		129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
		251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
		49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
		138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180,
		151, 160, 137, 91, 90, 15,
		131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
		190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
		88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
		77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
		102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
		135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
		5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
		223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
		129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
		251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
		49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
		138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180};
}

double NewNoise::fade(double t)
{
	return t * t * t * (t * (t * 6 - 15) + 10);
}

int NewNoise::floor(double x)
{
	return x < 0 ? (int) x - 1 : (int) x;
}

void NewNoise::sortCoords(double * coords[], char n)
{
	double * tmp;

	for (int i = 1; i < n; i++)
	{
		for (int j = 0; j < n - i; j++)
		{
			if (*coords[j] < *coords[j + 1])
			{
				tmp = coords[j];
				coords[j] = coords[j + 1];
				coords[j + 1] = tmp;
			}
		}
	}
}

double NewNoise::grad(unsigned char hash, double x, double y)
{
	double u = hash & 2 ? x : y;

	return (((hash & 1) ? -u : u) + 1) * 0.5;
}

double NewNoise::grad(unsigned char hash, double x, double y, double z)
{
	int h = hash & 15;
	double u = h < 8 ? x : y;
	double v = h < 4 ? y : h == 12 || h == 14 ? x : z;

	return (((h & 1) ? -u : u) + ((h & 2) ? -v : v) + 2) * 0.25;
}

double NewNoise::grad(unsigned char hash, double x, double y, double z, double w)
{
	int h = hash & 31;
	double u = h < 24 ? x : y;
	double v = h < 16 ? y : z;
	double t = h < 8 ? z : w;

	return (((h & 1) ? -u : u) + ((h & 2) ? -v : v) + ((h & 4) ? -t : t) + 3) * (1 / 6.);
}

double NewNoise::grad(unsigned char hash, double x, double y, double z, double w, double t)
{
	int h = hash & 127 % 80;
	double u = h < 64 ? x : y;
	double v = h < 48 ? y : z;
	double j = h < 32 ? z : w;
	double k = h < 16 ? w : t;

	return (((h & 1) ? -u : u) + ((h & 2) ? -v : v) + ((h & 4) ? -j : j) + ((h & 8) ? -k : k) + 4) * 0.125;
}