#pragma once

#include <std.h>

class NewNoise
{
	unsigned char * permTable;

	double fade(double t);
	int floor(double x);
	void sortCoords(double * coords[], char n);

	double grad(unsigned char hash, double x, double y);
	double grad(unsigned char hash, double x, double y, double z);
	double grad(unsigned char hash, double x, double y, double z, double w);
	double grad(unsigned char hash, double x, double y, double z, double w, double t);

	int octaves;
	double scale;
public:
	NewNoise(int octaves, double scale);

	double noise(int x, int y);
	double noise(int x, int y, int z);
	double noise(int x, int y, int z, int w);
	double noise(int x, int y, int z, int w, int t);

	double raw(double x, double y);
	double raw(double x, double y, double z);
	double raw(double x, double y, double z, double w);
	double raw(double x, double y, double z, double w, double t);
};